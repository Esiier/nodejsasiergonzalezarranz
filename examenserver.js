var express = require('express');

var bodyParser = require('body-parser');

const fileType = require('file-type');


var app = express();

app.use(bodyParser.json());

var ListaPost=[];

app.get('/', function(req, res) {
    res.end();
});

//Especificación 2: (3p)

app.post('/removeduplicatewords',function(req,res){
    if(req.body.palabras!=null){
        var finalString='';
        var palabraTemp='';
        for(var x=0;req.body.palabras.length>x;x++){
            if(req.body.palabras[x]==','){
                var rep=false;
                for(var i=0;ListaPost.length>i;i++){
                    if(palabraTemp==ListaPost[i]){
                        rep=true;
                    }
                }
                if(!rep){
                    ListaPost.push(palabraTemp);
                }
                palabraTemp='';
            }else{
                palabraTemp+=req.body.palabras[x];
            }
        }
        for(var x=0;ListaPost.length>x;x++){
            finalString+=ListaPost[x];
            if(x<ListaPost.length-1)
            {
                finalString+=','
            }
        }
    }
        res.write(finalString);
    res.end();
});


//Especificación 3: (3p)

app.post('/detectfiletype',function(req,res){
    

        
    const http = require('http');
 
    const url = req.body.url;
    var stringJson;
    http.get(url, response => {
        response.on('readable', () => {
            const chunk = response.read(fileType.minimumBytes);
            response.destroy();
            stringJson=(JSON.stringify(fileType(chunk)));
            console.log('file:',stringJson);
        });
    });
    
    /*
    var url = req;
    var response = fetch(url);
    var fileTyype = filetype.fromStream(response.body);
    var stream = got.createReadStream(url);

    res.send(filetype.fromStream(stream));
    res.send(fileTyype);
    */
    res.end()
});


//Especificación 4: (3p)

var botorders=[];

app.get('/botorder/:order',function(req,res){
    if(req.params.order!=null){
        console.log("Orden:",req.params.order);
        var actualBotOrder={orderName:req.params.order,botorder:'NONE'};
        for(var bot=0;botorders.length>bot;bot++){
            if(botorders[bot].orderName==actualBotOrder.orderName){
                actualBotOrder.botorder=botorders[bot].botorder;
            }
        }
        res.write(actualBotOrder.botorder);
    }
    res.end()
});

app.post('/botorder/:order',function(req,res){
    if(req.params.order!=null){
        res.write('OK');
        var botOrderEncontrada=false;
        for(var bot=0;botorders.length>bot;bot++){
            if(botorders[bot].orderName==req.params.order){
                botorders[bot].botorder=req.body.botorder;
                botOrderEncontrada=true;
            }
        }
        if(!botOrderEncontrada){
            var newBotOrder={orderName:req.params.order,botorder:req.body.botorder};
            botorders.push(newBotOrder);
        }
    }

    res.end();
});



app.listen(8000);
console.log('Listening port 8000');